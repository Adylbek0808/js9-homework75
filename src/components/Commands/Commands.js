import { Grid, IconButton, makeStyles, TextField, Typography } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import React from 'react';
import Wrapper from '../UI/Wrapper';

const useStyles = makeStyles(theme => ({
    iconSquare: {
        borderRadius: "10%",
        margin: theme.spacing('auto', 1)
    }
}));

const Commands = ({ currentPass, name, onChange, encode, decode }) => {
    const classes = useStyles();
    return (
        <Wrapper>
            <Grid item container alignItems='center'>
                <Grid item xs={4}>
                    <Typography variant="h6" >Encryption password:</Typography>
                </Grid>
                <Grid item container xs={8}>
                    <TextField
                        placeholder="Password"
                        variant="outlined"
                        defaultValue={currentPass}
                        name={name}
                        onChange={onChange}
                    />
                    <IconButton className={classes.iconSquare} centerRipple={false} onClick={encode}>
                        <ArrowDownwardIcon />
                    </IconButton>
                    <IconButton className={classes.iconSquare} centerRipple={false} onClick={decode}>
                        <ArrowUpwardIcon />
                    </IconButton>
                </Grid>

            </Grid>
        </Wrapper>
    );
};

export default Commands;