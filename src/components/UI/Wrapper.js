import Grid from '@material-ui/core/Grid';
import makeStyles from '@material-ui/core/styles/makeStyles';
import React from 'react';

const useStyles = makeStyles(theme => ({
    innerFields: {
        padding: theme.spacing(2)
    }
}));

const Wrapper = props => {
    const classes = useStyles();
    return (
        <Grid item container className={classes.innerFields} >
            {props.children}
        </Grid>
    );
};

export default Wrapper;