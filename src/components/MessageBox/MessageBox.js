import { Grid, TextField, Typography } from '@material-ui/core';
import React from 'react';
import Wrapper from '../UI/Wrapper';

const MessageBox = ({ title, message, onChange, name }) => {
    return (
        <Wrapper>
            <Grid item xs={4}>
                <Typography variant="h6" >{title}</Typography>
            </Grid>
            <Grid item xs={8}>
                <TextField variant="outlined" fullWidth multiline={true} rows={5} value={message} name={name} onChange={onChange}/>
            </Grid>
        </Wrapper>
    );
};

export default MessageBox;