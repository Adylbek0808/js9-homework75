import { CHANGE_VALUES, DECODE_MESSAGE_SUCCESS, ENCODE_MESSAGE_SUCCESS } from "../actions/actions";

const initialState = {
    encoded: "",
    password: "",
    decoded: ""
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_VALUES:
            return { ...state, [action.info.name]: action.info.message }
        case ENCODE_MESSAGE_SUCCESS:
            return { ...state, encoded: action.encodedMessage }
        case DECODE_MESSAGE_SUCCESS:
            return { ...state, decoded: action.decodedMessage }
        default:
            return state;
    }
};

export default reducer;