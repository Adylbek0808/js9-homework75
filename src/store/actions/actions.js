import axiosApi from '../../axiosApi';


export const ENCODE_MESSAGE_SUCCESS = "ENCODE_MESSAGE_SUCCESS";
export const DECODE_MESSAGE_SUCCESS = "DECODE_MESSAGE_SUCCESS";
export const CHANGE_VALUES = "CHANGE_VALUES"


export const encodeMessageSuccess = encodedMessage => ({ type: ENCODE_MESSAGE_SUCCESS, encodedMessage });
export const decodeMessageSuccess = decodedMessage => ({ type: DECODE_MESSAGE_SUCCESS, decodedMessage })
export const changeValues = info => ({ type: CHANGE_VALUES, info })

export const encodeMessage = toEncode => {
    return async dispatch => {
        const response = await axiosApi.post("/encode/", toEncode);
        console.log(response.data)
        dispatch(encodeMessageSuccess(response.data.encoded));
    }
}
export const decodeMessage = toDecode => {
    return async dispatch => {
        const response = await axiosApi.post("/decode/", toDecode);
        console.log(response.data)
        dispatch(decodeMessageSuccess(response.data.decoded));
    }
}
