import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import MessageBox from './components/MessageBox/MessageBox';
import Commands from './components/Commands/Commands';
import { useDispatch, useSelector } from 'react-redux';
import { changeValues, decodeMessage, encodeMessage } from './store/actions/actions';

const useStyles = makeStyles(theme => ({
  baseGrid: {
    margin: theme.spacing(0, 'auto')
  },
  titleStyle: {
    margin: theme.spacing(2, 0)
  }
}));

const App = () => {
  const classes = useStyles();
  const { encoded, decoded, password } = useSelector(state => state);

  const dispatch = useDispatch();

  const onChange = event => {
    const info = {
      name: event.target.name,
      message: event.target.value
    };
    dispatch(changeValues(info));
  }

  const sendMessageToEncode = () => {
    if (password.length === 0) {
      alert("Enter password!")
    } else {
      const message = {
        password: password,
        message: decoded
      }
      dispatch(encodeMessage(message));
    }

  }
  const sendMessageToDecode = () => {
    if (password.length === 0) {
      alert("Enter password!")
    } else {
      const message = {
        password: password,
        message: encoded
      }
      dispatch(decodeMessage(message));
    }
  }

  return (
    <>
      <CssBaseline />
      <Grid item container direction="column" spacing={2} xs={8} className={classes.baseGrid}>
        <Typography variant="h3" align="center" className={classes.titleStyle}>Message encryption</Typography>
        <Paper>
          <MessageBox title="Decoded message" message={decoded} name="decoded" onChange={onChange} />
          <Commands name="password" currentPass={password} onChange={onChange} encode={sendMessageToEncode} decode={sendMessageToDecode} />
          <MessageBox title="Encoded message" message={encoded} name="encoded" onChange={onChange} />
        </Paper>

      </Grid>
    </>
  );
}

export default App;
