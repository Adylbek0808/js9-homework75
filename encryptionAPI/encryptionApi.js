const express = require('express');
const cors=require('cors');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
app.use(express.json());
app.use(cors());
const port = 8000;


app.get('/', (req, res) => {
    res.send("We are live!")
});

app.post('/encode/', (req, res) => {
    const message = req.body;
    const response = {
        encoded: Vigenere.Cipher(message.password).crypt(message.message)
    }
    res.send(response);
});

app.post('/decode/', (req, res) => {
    const message = req.body;
    const response = {
        decoded: Vigenere.Decipher(message.password).crypt(message.message)
    }
    res.send(response);
});

app.listen(port, () => {
    console.log("Live at port: " + port)
})